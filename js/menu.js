import { signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';
import { auth } from './app.js';
import { storage } from './app.js';
import { ref, uploadBytes, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

const loginButton = document.querySelector('#uploadButton');
const errorMessage = document.getElementById('error-message');

const fileInput = document.getElementById('imageInput');
const imagePreview = document.getElementById('imagePreview');
const imageUrl = document.getElementById('imageUrl');
const uploadButton = document.getElementById('uploadButton');
const imagen = document.getElementById('imagen');

// Inicializa Firebase Auth y Firebase Storage
fileInput.addEventListener('change', function (event) {
  const file = event.target.files[0];
  if (file) {
    const reader = new FileReader();

    reader.onload = function (e) {
      imagePreview.src = e.target.result;
      imagePreview.style.display = 'block';
      imageUrl.value = e.target.result;
      uploadButton.style.display = 'block';
    };

    reader.readAsDataURL(file);
  } else {
    imagePreview.src = '';
    imagePreview.style.display = 'none';
    imageUrl.value = '';
    uploadButton.style.display = 'none';
  }
});

uploadButton.addEventListener('click', async () => {
  const file = fileInput.files[0];
  if (file) {
    try {
      const storageRef = ref(storage, 'imagenes/' + file.name);
      await uploadBytes(storageRef, file);

      alert('Imagen subida con éxito a Firebase Storage.'); // Mensaje de éxito

      // Obtener la URL de descarga y mostrarla (puedes mantener este código si lo deseas)
      const downloadURL = await getDownloadURL(storageRef);
      alert('URL de descarga: ' + downloadURL);
    } catch (error) {
      console.error('Error al cargar la imagen: ' + error.message);
      alert('Error al cargar la imagen. Por favor, inténtalo de nuevo.');
    }
  } else {
    console.error('No se seleccionó ningún archivo');
    alert('Por favor, selecciona un archivo antes de cargarlo.');
  }
});
